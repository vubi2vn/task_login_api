﻿using System;
using System.Collections.Generic;

namespace task_Login.Models
{
    public partial class NotificationSend
    {
        public string Id { get; set; }
        public int NotificationId { get; set; }
        public bool IsSent { get; set; }
        public DateTimeOffset? SentDate { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDelete { get; set; }
    }
}
