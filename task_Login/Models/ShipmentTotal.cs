﻿using System;
using System.Collections.Generic;

namespace task_Login.Models
{
    public partial class ShipmentTotal
    {
        public string TenCchq { get; set; }
        public string CustomMaNo { get; set; }
        public DateTime? CustomMaDate { get; set; }
        public DateTimeOffset? ClearanceDate { get; set; }
        public string ProductName { get; set; }
        public string MaNk { get; set; }
        public string QuocGia { get; set; }
        public double? ExchangeRate { get; set; }
        public double? Quantity { get; set; }
        public string Unit { get; set; }
        public double? Price { get; set; }
        public decimal? TriGiaKbNt { get; set; }
        public decimal? TriGiaKbVnd { get; set; }
        public string Curency { get; set; }
        public double? Cpbhiem { get; set; }
        public double? Cpvchuyen { get; set; }
        public string ContractNo { get; set; }
        public DateTime? ContractDate { get; set; }
        public string InvoiceNo { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public string MaBieuThue { get; set; }
        public string CobenThu3 { get; set; }
        public string Conumber { get; set; }
        public DateTime? Codate { get; set; }
        public double? ThueNk { get; set; }
        public double? TienThueNk { get; set; }
        public double? ThueVat { get; set; }
        public double? TienThueVat { get; set; }
        public double? TongThue { get; set; }
        public bool? HqdieuChinh { get; set; }
        public DateTime? ArriveDate { get; set; }
        public DateTime? TbhdenDate { get; set; }
        public DateTimeOffset? InstockDate { get; set; }
        public string CertNo { get; set; }
        public DateTime? CertDate { get; set; }
        public string HoaDon { get; set; }
        public DateTimeOffset? MovingDate { get; set; }
        public string Remark { get; set; }
        public string ImportCerNo { get; set; }
        public DateTimeOffset? ImportCerDate { get; set; }
        public double? TongSoLuong { get; set; }
        public decimal? TongNte { get; set; }
        public decimal? TongTienVnd { get; set; }
        public double? TongBh { get; set; }
        public double? TongVc { get; set; }
        public decimal? TongThueNk { get; set; }
        public decimal? TongVat { get; set; }
        public decimal? TongTienThue { get; set; }
    }
}
