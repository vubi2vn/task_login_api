﻿using System;
using System.Collections.Generic;

namespace task_Login.Models
{
    public partial class InternalOrder
    {
        public int Id { get; set; }
        public string InternalOrderCode { get; set; }
        public int? SupplierId { get; set; }
        public int? ById { get; set; }
        public int? StoreId { get; set; }
        public int? HarborTo { get; set; }
        public int? StatusId { get; set; }
        public string Note { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDelete { get; set; }
        public DateTimeOffset? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTimeOffset? ModifyDate { get; set; }
        public int? ModifyBy { get; set; }
    }
}
