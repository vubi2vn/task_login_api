﻿using System;
using System.Collections.Generic;

namespace task_Login.Models
{
    public partial class SupplierContractQuantityRemaining
    {
        public int? SupplierContractId { get; set; }
        public string ContractNo { get; set; }
        public int? ProductId { get; set; }
        public string ProductName { get; set; }
        public double? UnitPrice { get; set; }
        public string Currency { get; set; }
        public double? ContractQuantity { get; set; }
        public decimal? TotalShipmentQuantity { get; set; }
        public string PackageUnitName { get; set; }
        public string CountryName { get; set; }
        public string ProductByContractId { get; set; }
        public double? RemainingQuantity { get; set; }
    }
}
