﻿using System;
using System.Collections.Generic;

namespace task_Login.Models
{
    public partial class ProductList
    {
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string SuggestedCorrectName { get; set; }
        public string Supplier { get; set; }
        public string Origin { get; set; }
        public string Note { get; set; }
    }
}
