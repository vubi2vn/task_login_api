import { Injectable } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { HttpClient, HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private fb: FormBuilder, private http: HttpClient) { }
  readonly BaseURL = 'http://localhost:44354/api';

  getUserProfile() {
    return this.http.get(this.BaseURL + '/users');
  }
  getUser(id: Number) {
    console.log(this.BaseURL + "/users/" + id)
    return this.http.get(this.BaseURL + "/users/" + id);
  }
}
