import { Component, OnInit } from '@angular/core';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-task-login',
  templateUrl: './task-login.component.html',
  styleUrls: ['./task-login.component.css']
})
export class TaskLoginComponent implements OnInit {

  constructor(private router: Router, private toastr: ToastrService) { }
  
  ngOnInit(): void {
    if (localStorage.getItem('token') != null)
      this.router.navigateByUrl('*');
  }

  onSubmit(form: any): void {
    fetch('https://localhost:44354/api/login?username='+form.username+'&password='+form.password,{
      method:'POST',
  }).then(response => response.json()).then(data=>{
    if(data['token'] != null){
      localStorage.setItem('token',data['token']);
      localStorage.setItem('userid',data['userid']);
      this.router.navigateByUrl('/home');
    }
    else
     alert(data['message'])
    }).catch( (error)=>{
      alert(error);
      })
  }

}
