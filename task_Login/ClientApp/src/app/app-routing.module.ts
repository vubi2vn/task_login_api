import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TaskLoginComponent } from './task-login/task-login.component';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './auth/auth.guard';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component'

const routes: Routes = [
  { path: '', component: TaskLoginComponent },
  { path: 'login', component: TaskLoginComponent },
  { path: 'home', component: HomeComponent, canActivate:[AuthGuard] },
  {path: '**', component: PageNotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
