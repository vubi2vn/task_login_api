import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../shared/user.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  userDetails;
  constructor(private router: Router, private service: UserService, private userService: UserService) { }

  ngOnInit(): void {
    this.loadData()
  }

  user:any;
  loadData(){
    let id:Number = +localStorage.getItem('userid');
    this.userService.getUser(id).subscribe(data => {
      this.user = data;
    })
  }

  onlogout(){
    localStorage.removeItem('token');
    this.router.navigate(['login'])
  }
}
