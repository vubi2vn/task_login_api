﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.IdentityModel.Tokens;
using task_Login.Models;
using task_Login.Utils;

namespace task_Login.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly SIMMSContext _context;
        public LoginController(SIMMSContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult Login([FromQuery] string username, [FromQuery] string password)
        {
            if(username == null || password == null)
            {
                return BadRequest(new { message = "Vui lòng điền đầy đủ thông tin" });
            }

            var user = GetUser(username, password);
            if (user != null)
            {
                int userid = user.UserId;
                string token = GenerateToken(user);
                return Ok(new { token, userid });
            }
            else
            {
                return BadRequest(new { message = "Sai tên tài khoản hoặc mật khẩu" });
            }
        }

        //Generate jwt
        private string GenerateToken(User user)
        {
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim("UserId", user.UserId.ToString())
                    }),
                Expires = DateTime.UtcNow.AddDays(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Constant.JWT_Secret.ToString())), SecurityAlgorithms.HmacSha256Signature)
            };
            var tokenHandler = new JwtSecurityTokenHandler();
            var securityToken = tokenHandler.CreateToken(tokenDescriptor);
            var token = tokenHandler.WriteToken(securityToken);
            return token;
        }

        //MD5
        private string MD5(string password)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(password));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }

        private User GetUser(string username, string password)
        {
            return _context.User.FirstOrDefault(x => x.UserName == username && x.Password == MD5(password));
        }

    }
    }
